# mybatis-generator-javatyperesolver

#### 介绍
mybatis generator 代码生成器生成 bit tinyint smallint 等数值类型时, 会根据位数生成对应的 Java Boolean Byte Short 等类型, 使用着不太方便, 而且类型不能自定义, 这里复制了原有的 JavaTypeResolverDefaultImpl 代码, 扩展了 forceIntegers 属性, 用于强制生成 Java Integer 类型, 使用配置如下:

> pom.xml 配置

```
            ......
            <plugin>
                <groupId>org.mybatis.generator</groupId>
                <artifactId>mybatis-generator-maven-plugin</artifactId>
                <version>1.3.7</version>
                <configuration>
                    <configurationFile>${basedir}/src/main/resources/generator/generatorConfig.xml</configurationFile>
                    <overwrite>true</overwrite>
                    <verbose>true</verbose>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>top.ymfswyh</groupId>
                        <artifactId>mybatis-generator-javatyperesolver</artifactId>
                        <version>1.0.0</version>
                    </dependency>
                    <dependency>
                        <groupId>mysql</groupId>
                        <artifactId>mysql-connector-java</artifactId>
                        <version>${mysql.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            ......
```

> generatorConfig.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration
        PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">
<generatorConfiguration>
    <properties resource="generator/mgb-config.properties" />

    <context id="Mysql" targetRuntime="MyBatis3Simple"
             defaultModelType="flat">
        <property name="beginningDelimiter" value="`" />
        <property name="endingDelimiter" value="`" />

        <plugin type="${mapper.plugin}">
            <!-- 配置后生成的 Mapper 接口都会自动继承该接口 -->
            <property name="mappers" value="${mapper.Mapper}" />
            <!-- caseSensitive 是否区分大小写，默认值 false。
                如果数据库区分大小写，这里就需要配置为 true，
                这样当表名为 USER 时，会生成 @Table(name = "USER") 注解，否则使用小写 user 时会找不到表。
            -->
            <!--<property name="caseSensitive" value="false"/>-->
            <!-- forceAnnotation 是否强制生成注解，默认 false，
                如果设置为 true，不管数据库名和字段名是否一致，
                都会生成注解（包含 @Table 和 @Column）。
            -->
            <!--<property name="forceAnnotation" value="false"/>-->
            <!-- beginningDelimiter 和 endingDelimiter 开始和结束分隔符，对于有关键字的情况下适用 -->
            <!--<property name="beginningDelimiter" value="`"/>
            <property name="endingDelimiter" value="`"/>-->
        </plugin>

        <jdbcConnection driverClass="${jdbc.driverClass}"
                        connectionURL="${jdbc.url}" userId="${jdbc.user}" password="${jdbc.password}">
        </jdbcConnection>

        <javaTypeResolver type="top.ymfswyh.mybatis.generator.JavaTypeResolverDefaultImpl">
            <property name="forceIntegers" value="true"/>
            <property name="forceBigDecimals" value="true"/>
            <!-- This property is used to specify whether MyBatis Generator should force the use of JSR-310 data types for DATE, TIME,
            and TIMESTAMP fields, rather than using java.util.Date -->
            <property name="useJSR310Types" value="true"/>
        </javaTypeResolver>

        <javaModelGenerator targetPackage="${targetModelPackage}"
                            targetProject="${targetJavaProject}" />

        <sqlMapGenerator targetPackage="${targetXMLPackage}"
                         targetProject="${targetResourcesProject}" />

        <javaClientGenerator targetPackage="${targetMapperPackage}"
                             targetProject="${targetJavaProject}" type="XMLMAPPER" />

        <table tableName="sbg_user" domainObjectName="User">
            <generatedKey column="id" sqlStatement="Mysql" identity="true" />
        </table>
        <table tableName="sbg_order" domainObjectName="Order">
            <generatedKey column="id" sqlStatement="Mysql" identity="true" />
        </table>
    </context>
</generatorConfiguration>
```